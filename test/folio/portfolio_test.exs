defmodule Blackfolio.PortfolioTest do
  use Blackfolio.DataCase

  alias Blackfolio.Portfolio

  describe "list_holdings/1" do
    test "sums all amount by same assets" do
      create_transaction(%{pair: "BTC/USD", trade_price: 52000, quantity: 0.5})
      create_transaction(%{pair: "BTC/USD", trade_price: 52001, quantity: 0.01})

      assert Enum.member?(Portfolio.list_holdings(1), {:btc, 0.51})
      assert Enum.member?(Portfolio.list_holdings(1), {:usd, -26_520.01})
    end

    test "sums all amount by different assets" do
      create_transaction(%{pair: "BTC/USD", trade_price: 52000, quantity: 0.5})
      create_transaction(%{pair: "ETH/USD", trade_price: 2600, quantity: 4.0})

      assert Enum.member?(Portfolio.list_holdings(1), {:btc, 0.5})
      assert Enum.member?(Portfolio.list_holdings(1), {:eth, 4.0})
      assert Enum.member?(Portfolio.list_holdings(1), {:usd, -(52_000 * 0.5 + 2600 * 4.0)})
    end
  end

  def create_transaction(overrides \\ %{}) do
    params =
      %{
        pair: "BTC/USD",
        trade_price: 110.00,
        quantity: 2,
        side: "buy",
        trade_date: NaiveDateTime.utc_now()
      }
      |> Map.merge(overrides)

    Portfolio.create_transaction(params)
  end
end
