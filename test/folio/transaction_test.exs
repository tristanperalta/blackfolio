defmodule Blackfolio.TransactionTest do
  use Blackfolio.DataCase

  alias Blackfolio.Repo
  alias Blackfolio.Portfolio
  alias Portfolio.Transaction

  describe "create_transaction" do
    test "persist transaction" do
      params = %{
        pair: "BTC/USD",
        trade_price: 110.00,
        quantity: 2,
        current_price: 230.00,
        total_value: 2 * 10.00,
        side: "buy",
        trade_date: NaiveDateTime.utc_now()
      }

      assert {:ok, %{transaction: %Transaction{}}} = Portfolio.create_transaction(params)
    end

    test "computes total value" do
      params = %{
        pair: "BTC/USD",
        trade_price: 110.00,
        quantity: 2,
        current_price: 230.00,
        total_value: 2 * 10.00,
        side: "buy",
        trade_date: NaiveDateTime.utc_now()
      }

      assert {:ok, %{transaction: %Transaction{total_value: total_value}}} =
               Portfolio.create_transaction(params)

      assert Decimal.eq?(total_value, Decimal.new("220.0"))
    end

    test "buy BTC creates double-entry, an entry for BTC and USD" do
      # Buy 2 BTC @ Price 110 USD/BTC
      # spent = 2 * 110 = -220 USD
      # get = +2 BTC

      params = %{
        pair: "BTC/USD",
        trade_price: 110.00,
        quantity: 2,
        current_price: 110.00,
        side: "buy",
        trade_date: NaiveDateTime.utc_now()
      }

      assert {:ok, %{transaction: transaction}} =
               Portfolio.create_transaction(params)

      %{entries: entries} = Repo.preload(transaction, [:entries])

      assert Decimal.eq?(get_total_asset(entries, "USD"), Decimal.new("-220.0"))
      assert Decimal.eq?(get_total_asset(entries, "BTC"), Decimal.new("2"))
    end

    test "sell BTC creates double-entry, an entry for BTC and USD" do
      # Sell 2 BTC @ Price 110 USD/BTC
      # spent = 2 * 110 = +220 USD
      # get = -2 BTC

      params = %{
        pair: "BTC/USD",
        trade_price: 110.00,
        quantity: 2,
        current_price: 110.00,
        side: "sell",
        trade_date: NaiveDateTime.utc_now()
      }

      assert {:ok, %{transaction: transaction}} =
               Portfolio.create_transaction(params)

      %{entries: entries} = Repo.preload(transaction, [:entries])
      assert Decimal.eq?(get_total_asset(entries, "BTC"), Decimal.new("-2"))
      assert Decimal.eq?(get_total_asset(entries, "USD"), Decimal.new("220.0"))
    end
  end

  defp get_total_asset(entries, asset) do
    entries
    |> Enum.group_by(& &1.asset)
    |> Map.get(asset)
    |> Enum.map(& &1.amount)
    |> Enum.reduce(Decimal.new("0"), fn amt, acc ->
      Decimal.add(acc, amt)
    end)
  end
end
