defmodule Blackfolio.Binance.Websocket do
  use WebSockex

  @base_endpoint "wss://stream.binance.com:9443"

  def start_link(stream_names) when is_list(stream_names) do
    query_string = "stream?streams=#{Enum.join(stream_names, "/")}"
    WebSockex.start_link("#{@base_endpoint}/#{query_string}", __MODULE__, %{})
  end

  # stream name e.g. "BNBBTC@trade"
  def start_link(stream_name) do
    WebSockex.start_link("#{@base_endpoint}/ws/#{stream_name}", __MODULE__, %{})
  end

  def handle_frame({_type, msg}, state) do
    # IO.puts "Received Message - Type: #{inspect type} -- Message: #{inspect Jason.decode!(msg)}"

    Phoenix.PubSub.broadcast(
      Blackfolio.PubSub,
      "ticker",
      Jason.decode!(msg)
    )

    {:ok, state}
  end

  def handle_cast({:send, {type, msg} = frame}, state) do
    IO.puts("Sending #{type} frame with payload: #{msg}")
    {:reply, frame, state}
  end
end
