defmodule Blackfolio.Binance.Client do
  @base_url "https://api.binance.com"

  use HTTPoison.Base

  def get_instruments() do
    get("/exchangeInfo")
  end

  def process_request_url(url) do
    "#{@base_url}/api/v3" <> url
  end

  def process_response_body(binary) do
    try do
      Jason.decode!(binary)
    rescue
      _e -> binary
    end
  end
end
