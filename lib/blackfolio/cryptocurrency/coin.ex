defmodule Blackfolio.Cryptocurrency.Coin do

  use Ecto.Schema

  import Ecto.Changeset

  @primary_key {:id, :string, []}

  schema "coins" do
    field :name, :string
    field :symbol, :string
  end

  @required [:name, :symbol]
  @permitted @required
  def changeset(coin, attrs) do
    coin
    |> cast(attrs, @permitted)
    |> validate_required(@required)
  end
end
