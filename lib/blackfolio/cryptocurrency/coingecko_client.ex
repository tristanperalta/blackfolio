defmodule Blackfolio.Cryptocurrency.CoingeckoClient do
  use HTTPoison.Base

  def list_coins() do
    get("/coins/list")
  end

  def process_request_url(url) do
    "https://api.coingecko.com/api/v3" <> url
  end

  def process_response_body(binary) do
    try do
      Jason.decode!(binary)
    rescue
      _e -> binary
    end
  end
end
