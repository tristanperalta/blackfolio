defmodule Blackfolio.Cryptocurrency do
  import Ecto.Query

  alias Blackfolio.Repo
  alias __MODULE__.Coin
  alias __MODULE__.CoingeckoClient

  def pull_coins() do
    case CoingeckoClient.get("/coins/list") do
      {:ok, %HTTPoison.Response{body: body}} ->
        body
      _ -> {:error, :could_not_retrieve_list}
    end
  end

  def save_coins() do
    pull_coins()
    |> Enum.map(&Map.new(&1, fn {key, val} ->
      {String.to_atom(key), val}
    end))
    |> then(&Repo.insert_all(
      Coin, &1, []))
  end

  def search_coin(search_term) do
    query =
      from(c in Coin,
        where: fragment(
          "to_tsvector(concat_ws(' ', ?, ?, ?)) @@ to_tsquery(?)",
          c.id, c.name, c.symbol, ^search_term)
      )

    Repo.all(query)
  end
end
