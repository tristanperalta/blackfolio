defmodule Blackfolio.Portfolio do
  alias Blackfolio.Repo
  alias __MODULE__.{Transaction, Entry}

  @moduledoc """
  Portfolio

  Display the summary of user portfolio. It calculates total value, percentage
  change and individual asset details

  # Example output
  ```
  {
    total: 190000,
    assets: [
      {:btc, {price: 52000, amount: 1, value: 52000}},
      {:eth, {price: 2600, amount: 1, value: 52000}},
      {:bnb, {price: 300, amount: 1, value: 52000}},
    ]
  }
  ```
  """

  def get_portfolio(assets) do
    %{total: compute_total(assets), assets: assets}
  end

  def list_assets(_portfolio) do
    Entry
    |> Repo.all()
    |> Repo.preload([:transaction])
    |> Enum.group_by(& &1.asset)
    |> Enum.reduce([], fn {coin, entries}, acc ->
      last_price = get_latest_price(entries)
      total =
        Enum.reduce(entries, Decimal.new(0), fn x, acc ->
          Decimal.add(acc, x.amount)
        end)

      key = coin |> String.downcase() |> String.to_atom()
      Keyword.put(acc, key, %{
        amount: Decimal.to_float(total),
        last_price: last_price
      })
    end)
  end

  defp get_latest_price(entries) do
    entries
    |> Enum.sort_by(&
      &1
      |> Map.get(:transaction)
      |> Map.get(:inserted_at)
    )
    |> Enum.reverse()
    |> List.first()
    |> Map.get(:transaction)
    |> Map.get(:trade_price)
  end

  @doc """
  List of assets by coins, then computes the value against the current price

  e.g.
    [
      {:btc, %{price: 52000, amount: 1, value: 52000}},
      {:eth, %{price: 2300, amount: 2, value: 4600}},
      {:bnb, %{price: 52000, amount: 1, value: 52000}},
    ]
  """
  def list_computed_assets(assets, prices) do
    assets
    |> Enum.filter(fn {coin, _} -> coin !== :usd end)
    |> Enum.reduce([], fn {coin, %{amount: amount, last_price: last_price}}, acc ->
      price = Keyword.get(prices, coin, last_price)
      value = Decimal.mult(Decimal.from_float(amount), price)
      [{coin, %{amount: amount, value: value, price: price}} | acc]
    end)
    |> Enum.reverse()
  end

  def compute_total(portfolio) do
    Enum.reduce(portfolio, Decimal.new(0), fn {_, %{value: value}}, acc ->
      Decimal.add(acc, value)
    end)
  end

  def get_asset(portfolio, coin) do
    Keyword.get(portfolio, coin)
  end

  def update_asset_price(portfolio, coin, price) do
    %{amount: amount} = asset = get_asset(portfolio, coin)
    updated_asset = asset
      |> Map.put(:value, price * amount)
      |> Map.put(:price, price)

    Keyword.put(portfolio, coin, updated_asset)
  end

  def update_prices(portfolio, prices) do
    Enum.map(portfolio, fn {coin, holding} ->
      Map.put(holding, coin, Map.get(prices, coin, holding))
    end)
  end

  def create_transaction(params) do
    Ecto.Multi.new()
    |> Ecto.Multi.insert(:transaction, change_transaction(%Transaction{}, params))
    |> Ecto.Multi.insert(:entry1, fn %{transaction: transaction} ->
      [account1, _account2] = String.split(transaction.pair, "/")

      Ecto.build_assoc(transaction, :entries)
      |> Entry.changeset(%{amount: compute_amount(transaction), asset: account1})
    end)
    |> Ecto.Multi.insert(:entry2, fn %{transaction: transaction} ->
      [_account1, account2] = String.split(transaction.pair, "/")

      Ecto.build_assoc(transaction, :entries)
      |> Entry.changeset(%{amount: compute_cost(transaction), asset: account2})
    end)
    |> Repo.transaction()
  end

  defp compute_cost(%{side: "buy"} = transaction) do
    Decimal.mult(transaction.quantity, transaction.trade_price)
    |> Decimal.negate()
  end

  defp compute_cost(transaction) do
    Decimal.mult(transaction.quantity, transaction.trade_price)
  end

  defp compute_amount(%{side: "buy"} = transaction) do
    transaction.quantity
  end

  defp compute_amount(%{side: "sell"} = transaction) do
    Decimal.negate(transaction.quantity)
  end

  def delete_transaction(%Transaction{} = transaction) do
    transaction
    |> Repo.delete()
  end

  def list_transaction() do
    %Transaction{}
    |> Repo.all()
  end

  def change_transaction(%Transaction{} = transaction, attrs) do
    Transaction.changeset(transaction, attrs)
  end
end
