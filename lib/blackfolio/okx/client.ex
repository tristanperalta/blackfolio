defmodule Blackfolio.OKX.Client do
  @base_url "https://www.okx.com"

  use HTTPoison.Base

  def get_instruments() do
    get("/public/instruments", [], params: [instType: "SPOT"])
  end

  def process_request_url(url) do
    "#{@base_url}/api/v5" <> url
  end

  def process_response_body(binary) do
    try do
      Jason.decode!(binary)
    rescue
      _e -> binary
    end
  end
end
