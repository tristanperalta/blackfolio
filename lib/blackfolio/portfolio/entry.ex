defmodule Blackfolio.Portfolio.Entry do
  use Ecto.Schema

  import Ecto.Changeset

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id

  schema "entries" do
    field :amount, :decimal
    field :asset, :string

    belongs_to :transaction, Blackfolio.Portfolio.Transaction
  end

  @required ~w(amount asset)a
  @permitted [:transaction_id] ++ @required
  def changeset(entry, attrs \\ %{}) do
    entry
    |> cast(attrs, @permitted)
    |> validate_required(@required)
  end
end
