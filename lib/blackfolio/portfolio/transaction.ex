defmodule Blackfolio.Portfolio.Transaction do
  use Ecto.Schema

  import Ecto.Changeset

  @primary_key {:id, :binary_id, autogenerate: true}
  schema "transactions" do
    field :pair, :string
    field :exchange, :string
    field :side, :string, default: "buy"
    field :current_price, :decimal, virtual: true
    field :trade_price, :decimal
    field :quantity, :decimal, default: Decimal.new(0)
    field :total_value, :decimal, virtual: true

    field :trade_date, :naive_datetime,
      default: NaiveDateTime.utc_now() |> NaiveDateTime.truncate(:second)

    field :notes, :string

    timestamps()

    has_many :entries, Blackfolio.Portfolio.Entry
  end

  @required [:pair, :side, :trade_price, :quantity]
  @permitted ~w(exchange trade_date notes)a ++ @required

  def changeset(transaction, params \\ %{}) do
    transaction
    |> cast(params, @permitted)
    |> validate_required(@required)
    |> validate_number(:quantity, greater_than: 0)
    |> compute_total_value()
  end

  defp compute_total_value(%{valid?: true} = changeset) do
    changeset
    |> then(fn cs ->
      transaction = apply_changes(cs)
      trade_price = Map.get(transaction, :trade_price)
      quantity = Map.get(transaction, :quantity)

      put_change(cs, :total_value, Decimal.mult(trade_price, quantity))
    end)
  end

  defp compute_total_value(changeset), do: changeset
end
