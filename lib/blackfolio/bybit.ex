defmodule Blackfolio.Bybit do
  @behaviour Blackfolio.MarketData

  def get_instruments() do
    case Blackfolio.Bybit.Client.get_instruments() do
      {:ok, %HTTPoison.Response{body: %{"result" => %{"list" => list}}}} ->
        Enum.map(list, &to_model/1)

      _ -> {:error, :unable_to_get_instruments}
    end
  end

  defp to_model(original) do
    %Blackfolio.MarketData.Instrument{
      base_coin: Map.get(original, "baseCoin"),
      quote_coin: Map.get(original, "quoteCoin"),
      symbol: Map.get(original, "symbol"),
      status: Map.get(original, "status") |> get_status(),
      exchange: "bybit"
    }
  end

  # Reference: https://bybit-exchange.github.io/docs/v5/enum#status
  @status_mapping %{
    "PreLaunch" => :pre_trading,
    "Trading" => :trading,
    "Settling" => :closed,
    "Delivering" => :closed,
    "Closed" => :closed
  }

  defp get_status(status) do
    Map.get(@status_mapping, status, :unknown)
  end
end
