defmodule Blackfolio.MarketData.Instrument do

  use Ecto.Schema

  import Ecto.Changeset

  @primary_key {:id, :binary_id, autogenerate: true}
  schema "instruments" do
    field :base_coin, :string
    field :quote_coin, :string
    field :symbol, :string
    field :status, Ecto.Enum, values: [:trading, :pre_trading, :closed, :unknown]
    field :exchange, :string

    timestamps()
  end

  @required [:base_coin, :quote_coin, :status, :exchange]
  @permitted @required ++ [:status]
  def changeset(instrument, attrs) do
    instrument
    |> cast(attrs, @permitted)
    |> validate_required(@required)
  end
end
