defmodule Blackfolio.Binance do
  use GenServer

  def start_link(_) do
    GenServer.start_link(__MODULE__, [], name: __MODULE__)
  end

  @doc """
  list prices in usd
    usdrate = usdrate * 1
    [
      :btc, 52000 * usdrate,
      :eth, 2890 * usdrate,
      :bnb, 351 * usdrate
    ]

  list prices in php
    php/usd = 55

    [
      :btc, 52000 * phpusd,
      :eth, 2890 * phpusd,
      :bnb, 351 * phpusd
    ]

  """

  def list_prices() do
    GenServer.call(__MODULE__, :list_prices)
  end

  def get_price(coin) do
    GenServer.call(__MODULE__, {:get_price, coin})
  end

  def init(_opts) do
    Phoenix.PubSub.subscribe(Blackfolio.PubSub, "ticker")
    Process.send_after(__MODULE__, :price_update, 1_000)

    {:ok, %{prices: []}}
  end

  def handle_info(
        %{"data" => %{"e" => "24hrTicker", "s" => symbol, "w" => price}},
        %{prices: prices} = state
      ) do
    prices = Keyword.put(prices, symbol_to_key(symbol), Decimal.new(price))

    {:noreply, Map.put(state, :prices, prices)}
  end

  def handle_info(
        %{"data" => %{"e" => "trade", "s" => symbol, "p" => price}},
        %{prices: prices} = state
      ) do
    prices = Keyword.put(prices, symbol_to_key(symbol), Decimal.new(price))

    {:noreply, Map.put(state, :prices, prices)}
  end

  def handle_info(:price_update, %{prices: prices} = state) do
    Phoenix.PubSub.broadcast(Blackfolio.PubSub, "price_update", prices)
    Process.send_after(__MODULE__, :price_update, 1_000)
    {:noreply, state}
  end

  def handle_info(_, state), do: {:noreply, state}

  defp symbol_to_key(symbol) do
    symbol
    |> String.slice(0..2)
    |> String.downcase()
    |> String.to_atom()
  end

  def handle_call({:get_price, coin}, _from, %{prices: prices} = state) do
    {:reply, Keyword.get(prices, coin), state}
  end

  def handle_call(:list_prices, _from, %{prices: prices} = state) do
    {:reply, prices, state}
  end

  @behaviour Blackfolio.MarketData

  def get_instruments() do
    case Blackfolio.Binance.Client.get_instruments() do
      {:ok, %HTTPoison.Response{body: %{"symbols" => list}}} ->
        Enum.map(list, &to_model/1)

      _ -> {:error, :unable_to_get_instruments}
    end
  end

  defp to_model(original) do
    %Blackfolio.MarketData.Instrument{
      base_coin: Map.get(original, "baseAsset"),
      quote_coin: Map.get(original, "quoteAsset"),
      symbol: Map.get(original, "symbol"),
      status: original |> Map.get("status") |> get_status(),
      exchange: "binance"
    }
  end

  # Reference: https://binance-docs.github.io/apidocs/spot/en/#enum-definitions
  @status_mapping %{
    "PRE_TRADING" => :pre_trading,
    "TRADING" => :trading,
    "POST_TRADING" => :closed,
    "END_OF_DAY" => :closed,
    "HALT" => :closed,
    "AUCTION_MATCH" => :closed,
    "BREAK" => :closed,
  }

  defp get_status(status) do
    Map.get(@status_mapping, status, :unknown)
  end
end
