defmodule Blackfolio.OKX do
  @behaviour Blackfolio.MarketData

  def get_instruments() do
    case Blackfolio.OKX.Client.get_instruments() do
      {:ok, %HTTPoison.Response{body: %{"data" => list}}} ->
        Enum.map(list, &to_model/1)

      _ -> {:error, :unable_to_get_instruments}
    end
  end

  defp to_model(original) do
    %Blackfolio.MarketData.Instrument{
      base_coin: Map.get(original, "baseCcy"),
      quote_coin: Map.get(original, "quoteCcy"),
      symbol: Map.get(original, "instId"),
      status: Map.get(original, "state") |> get_status(),
      exchange: "okx"
    }
  end


  # Reference:
  # https://www.okx.com/docs-v5/en/#public-data-rest-api-get-instruments
  @status_mapping %{
    "live" => :trading,
    "suspend" => :closed,
    "preopen" => :pre_trading,
    "test" => :closed,
  }

  defp get_status(status) do
    Map.get(@status_mapping, status, :unknown)
  end
end
