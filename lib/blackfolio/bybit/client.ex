defmodule Blackfolio.Bybit.Client do
  @base_url "https://api.bybit.com"

  use HTTPoison.Base

  def get_instruments() do
    get("/market/instruments-info", [], params: [category: "linear"])
  end

  def get_trade_history(params \\ []) do
    get("/market/recent-trade", [], params: Keyword.put(params, :category, "spot"))
  end

  def process_request_url(url) do
    "#{@base_url}/v5" <> url
  end

  def process_response_body(binary) do
    try do
      Jason.decode!(binary)
    rescue
      _e -> binary
    end
  end
end
