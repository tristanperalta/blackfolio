defmodule Blackfolio.MarketData do
  import Ecto.Query, only: [where: 3]

  alias Blackfolio.Repo
  alias __MODULE__.Instrument

  @callback get_instruments() :: 
    [Instrument.t()] | {:error, :unable_to_get_instruments}

  def pull_all_instruments do
    [Blackfolio.Binance, Blackfolio.Bybit, Blackfolio.OKX]
    |> Enum.map(fn mod ->
      apply(mod, :get_instruments, [])
    end)
    |> List.flatten()
  end

  def save_all(instruments) do
    now = NaiveDateTime.utc_now() |> NaiveDateTime.truncate(:second)

    instruments
    |> Enum.map(&Map.from_struct/1)
    |> Enum.map(fn inst -> Map.delete(inst, :__meta__) end)
    |> Enum.map(fn inst -> 
      inst
      |> Map.put(:id, Ecto.UUID.generate())
      |> Map.put(:inserted_at, now)
      |> Map.put(:updated_at, now)
    end)
    |> then(&Repo.insert_all(Instrument, &1, on_conflict: :nothing))
  end

  def list_instruments() do
    Repo.all(Instrument)
  end

  def list_instruments(coin) do
    coin = String.upcase(coin)
    Instrument
    |> where([i], i.base_coin == ^coin)
    |> Repo.all()
  end
end
