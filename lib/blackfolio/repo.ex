defmodule Blackfolio.Repo do
  use Ecto.Repo,
    otp_app: :blackfolio,
    adapter: Ecto.Adapters.Postgres
end
