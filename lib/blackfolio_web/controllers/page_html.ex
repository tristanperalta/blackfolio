defmodule BlackfolioWeb.PageHTML do
  use BlackfolioWeb, :html

  embed_templates "page_html/*"
end
