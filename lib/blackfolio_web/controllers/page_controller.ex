defmodule BlackfolioWeb.PageController do
  use BlackfolioWeb, :controller

  def home(conn, _params) do
    # The home page is often custom made,
    # so skip the default app layout.
    render(conn, :home, layout: false)
  end

  def add(conn, _params) do
    conn
    |> assign(:form, %{})
    |> render(:add, layout: false)
  end

  def create(conn, _params) do
    redirect(conn, to: "/")
  end
end
