defmodule BlackfolioWeb.Layouts do
  use BlackfolioWeb, :html

  embed_templates "layouts/*"
end
