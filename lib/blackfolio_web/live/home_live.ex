defmodule BlackfolioWeb.HomeLive do
  require Logger

  use Phoenix.LiveView

  alias Blackfolio.Portfolio

  import BlackfolioWeb.CoreComponents

  @interval 5_000

  def mount(_, _, socket) do
    assets = Portfolio.list_assets(1)

    {:ok, socket
      |> assign(:assets, assets)
      |> update_portfolio()
    }
  end

  def handle_info(:tick, socket) do
    {:noreply, update_portfolio(socket)}
  end

  defp update_portfolio(socket) do
    prices = Blackfolio.Binance.list_prices()
    assets = Map.get(socket, :assigns) |> Map.get(:assets)
    assets = Portfolio.list_computed_assets(assets, prices)

    %{assets: assets, total: total} = Portfolio.get_portfolio(assets)
    total = format_money(total)
    assets = format_assets_money(assets)

    if connected?(socket), do: schedule_timer()

    socket
    |> assign(:portfolio, assets)
    |> assign(:prices, prices)
    |> assign(:total, total)
  end

  defp format_number(amt) do
    amt
    |> Decimal.new()
    |> Decimal.round(2)
    |> Decimal.to_string()
  end

  defp format_money(amt) when is_float(amt) do
    Decimal.from_float(amt) |> format_money()
  end

  defp format_money(%Decimal{} = decimal) do
    format_number(decimal)
    |> then(fn val -> "$#{val}" end)
  end

  defp format_assets_money(assets) do
    Enum.map(assets, fn {coin, details} ->
      {coin,
       details
       |> Map.update!(:value, &format_money/1)
       |> Map.update!(:price, &format_money/1)}
    end)
  end

  defp schedule_timer() do
    Process.send_after(self(), :tick, @interval)
  end

  def render(assigns) do
    ~H"""
    <div class="container mx-auto">
      <h1>Blackfolio</h1>
      <div class="flex p-3 bg-violet-800 text-slate-50 rounded-md">
        <div class="flex-auto">
          <div>Portfolio 1</div>
          <div class="text-3xl"><%= @total %></div>
        </div>
        <div class="w-auto">
          <span>24HR</span>
          <span class="text-green-600">+8.2%</span>
        </div>
      </div>

      <div class="flex p-3">
        <table class="flex-auto">
          <tr>
            <th class="text-left">Coin</th>
            <th class="text-right">Price</th>
            <th class="text-right">Holdings</th>
          </tr>
          <%= for {coin, %{price: price, amount: amount, value: value}} <- @portfolio do %>
            <tr>
              <td>
                <div class="flex gap-2"><.crypto_icon name={Atom.to_string(coin)} /> <%= coin %></div>
              </td>
              <td class="text-right">
                <div><%= price %></div>
                <div class="text-green-600">+4.2%</div>
              </td>
              <td class="text-right">
                <div><%= value %></div>
                <div><%= amount %></div>
              </td>
            </tr>
          <% end %>
        </table>
      </div>
      <a class="p-2 bg-blue-700 rounded" href="/coin_select">Add Coin</a>
    </div>
    """
  end
end
