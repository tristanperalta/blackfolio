defmodule BlackfolioWeb.CoinSelectLive do
  use Phoenix.LiveView

  import BlackfolioWeb.CoreComponents

  def mount(_params, _session, socket) do
    coins = [
      btc: "Bitcoin",
      eth: "Ethereum",
      bnb: "Binance Coin",
      ltc: "Litecoin"
    ]

    {:ok, assign(socket, :coins, coins)}
  end

  def handle_event("search", _params, socket) do
    {:noreply, socket}
  end

  def render(assigns) do
    ~H"""
    <div class="container mx-auto">
      <h1>Select Coin</h1>

      <form phx-submit="search" phx-change="search">
        <input type="text" placeholder="Search coin" phx-debounce="1000" />
        <button type="submit">Go</button>
      </form>

      <div class="flex flex-col">
        <%= for {coin, name} <- @coins do %>
          <.link navigate={"/add/#{coin}"}>
            <div class="flex gap-2"><.crypto_icon name={Atom.to_string(coin)} /> <%= name %></div>
          </.link>
        <% end %>
      </div>
    </div>
    """
  end
end
