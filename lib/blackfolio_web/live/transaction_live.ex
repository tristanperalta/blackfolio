defmodule BlackfolioWeb.TransactionLive do
  use Phoenix.LiveView

  import BlackfolioWeb.CoreComponents

  alias Blackfolio.Portfolio
  alias Portfolio.Transaction

  def mount(params, _session, socket) do
    coin = Map.get(params, "coin", "BTC")

    transaction = %Transaction{
      quantity: Decimal.new(0),
      current_price: Blackfolio.Binance.get_price(coin |> String.to_atom()),
      trade_price: Blackfolio.Binance.get_price(coin |> String.to_atom()),
      side: "buy",
      trade_date: NaiveDateTime.utc_now()
    }

    changeset = Transaction.changeset(transaction)
    current_price = get_current_price(socket, coin)

    {:ok,
     socket
     |> assign(:form, to_form(changeset))
     |> assign(:current_price, current_price)
     |> assign(:pair_options, get_pair_options(coin))
     |> put_private(:coin, coin)}
  end

  def handle_event("create", %{"transaction" => params}, socket) do
    case Portfolio.create_transaction(params) do
      {:ok, _} ->
        {:noreply, push_navigate(socket, to: "/")}

      {:error, :transaction, changeset, _} ->
        form =
          changeset
          |> Map.put(:action, :insert)
          |> to_form()

        {:noreply, assign(socket, :form, form)}

      {:error, _, _, _} ->
        {:noreply, socket}
    end
  end

  def handle_event("validate", %{"transaction" => transaction_params}, socket) do
    current_price = get_current_price(socket)

    form =
      %Transaction{}
      |> Transaction.changeset(transaction_params)
      |> Map.put(:action, :insert)
      |> to_form()

    {:noreply,
     socket
     |> assign(:form, form)
     |> assign(:current_price, current_price)}
  end

  def handle_event("trade_pair_changed", %{"transaction" => transaction_params}, socket) do
    current_price = get_current_price(socket)
    form =
      %Transaction{}
      |> Transaction.changeset(transaction_params |> Map.put("trade_price", current_price))
      |> Map.put(:action, :insert)
      |> to_form()

    {:noreply,
     socket
     |> assign(:form, form)
     |> assign(:current_price, current_price)}
  end

  defp get_current_price(socket) do
    get_current_price(socket, "btc")
  end

  defp get_current_price(socket, coin) do
    socket
    |> Map.get(:private)
    |> Map.get(:coin, coin)
    |> then(&Blackfolio.Binance.get_price(String.to_atom(&1)))
  end

  defp get_pair_options(coin) do
    Blackfolio.MarketData.list_instruments(coin)
    |> Enum.map(&Map.get(&1, :symbol))
  end

  def render(assigns) do
    ~H"""
    <div class="container mx-auto">
      <h1>Add Transaction</h1>
      <.form class="flex flex-col" for={@form} phx-change="validate" phx-submit="create">
        <fieldset>
          <div class="group">
            <label class="flex flex-row justify-between">
              Trading pair
              <.input
                type="select"
                field={@form[:pair]}
                options={@pair_options}
                phx-change="trade_pair_changed"
              />
            </label>
          </div>
          <div class="group">
            <label class="flex flex-row justify-between">
              Select exchange
              <.input
                type="select"
                field={@form[:exchange]}
                options={["Binance", "Coinbase", "Kraken"]}
              />
            </label>
          </div>
          <div class="group">
            <label>Side</label>
            <div class="flex flex-row gap-10">
              <.input
                label="Buy"
                type="radio"
                field={@form[:side]}
                value="buy"
                checked={@form[:side].value == "buy"}
              />
              <.input
                label="Sell"
                type="radio"
                field={@form[:side]}
                value="sell"
                checked={@form[:side].value == "sell"}
              />
            </div>
          </div>
        </fieldset>

        <fieldset>
          <div class="group">
            <label class="flex flex-row justify-between">
              Current price <span><%= @current_price %></span>
            </label>
          </div>
          <div class="group">
            <label class="flex flex-row justify-between">
              Trade price <.input type="text" field={@form[:trade_price]} />
            </label>
          </div>
          <div class="group">
            <label class="flex flex-row justify-between">
              Quantity <.input type="number" field={@form[:quantity]} />
            </label>
          </div>
          <div class="group">
            <label class="flex flex-row justify-between">
              Total value <.input type="text" field={@form[:total_value]} />
            </label>
          </div>
        </fieldset>

        <fieldset>
          <div class="group">
            <label class="flex flex-row justify-between">
              Trade date <.input type="datetime-local" field={@form[:trade_date]} />
            </label>
          </div>
          <div class="group">
            <label class="flex flex-row justify-between">
              Notes <.input type="textarea" field={@form[:notes]} />
            </label>
          </div>
        </fieldset>
        <a href="/">Dashboard</a>
        <.button class="p-2 bg-blue-700 rounded" type="submit">Save</.button>
      </.form>
    </div>
    """
  end
end
