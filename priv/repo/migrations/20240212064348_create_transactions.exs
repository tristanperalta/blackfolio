defmodule Blackfolio.Repo.Migrations.CreateTransactions do
  use Ecto.Migration

  def change do
    create table(:transactions, primary_key: false) do
      add(:id, :binary_id, primary_key: true)
      add(:pair, :string)
      add(:exchange, :string)
      add(:side, :string, null: false)
      add(:trade_price, :decimal, null: false)
      add(:quantity, :decimal, null: false)
      add(:trade_date, :naive_datetime)
      add(:notes, :string)

      timestamps()
    end
  end
end
