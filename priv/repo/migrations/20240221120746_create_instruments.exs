defmodule Blackfolio.Repo.Migrations.CreateInstruments do
  use Ecto.Migration

  def change do
    create table(:instruments, primary_key: false) do
      add :id, :binary_id, primary_key: true
      add :base_coin, :string, null: false
      add :quote_coin, :string, null: false
      add :symbol, :string, null: false
      add :status, :string
      add :exchange, :string, null: false

      timestamps()
    end
  end
end
