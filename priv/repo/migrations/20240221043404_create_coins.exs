defmodule Blackfolio.Repo.Migrations.CreateCoins do
  use Ecto.Migration

  def change do
    create table(:coins, primary_key: false) do
      add(:id, :string, primary_key: true)
      add :name, :string
      add :symbol, :string
    end
  end
end
