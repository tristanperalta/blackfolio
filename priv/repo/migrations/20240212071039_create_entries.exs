defmodule Blackfolio.Repo.Migrations.CreateEntries do
  use Ecto.Migration

  def change do
    create table(:entries, primary_key: false) do
      add :id, :binary_id, primary_key: true
      add :amount, :decimal
      add :asset, :string

      add :transaction_id, references(:transactions, type: :binary_id)
    end
  end
end
