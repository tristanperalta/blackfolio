# Blackfolio

Blackfolio is a portfolio tracker web application to manage and track your cryptocurrency investments.

## Features

* Track portfolio performance
* Can make multiple portfolios
* Real-time market data
* Alerts and notifications

## Requirements

* Elixir 1.16+
* Phoenix
* Postgresql

## Installation

```sh
git clone git@gitlab.com:tristanperalta/blackfolio.git
cd blackfolio
mix setup

iex -S mix phx.server
```

Open your browser and navigate to `https://localhost:4000`.
